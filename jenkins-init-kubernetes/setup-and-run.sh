#!/usr/bin/env bash

dockerhub_user=1renatodevops

jenkins_port=8080
image_name=jenkins-devops
image_version=2.1.2
container_name=md-jenkins

docker pull jenkins:2.176.1-jdk11

if [ ! -d downloads ]; then
	mkdir downloads
fi

docker stop ${container_name}

docker build --no-cache -t ${dockerhub_user}/${image_name}:${image_version} .

if [ ! -d m2deps ]; then
	mkdir m2deps
fi
if [ -d jobs ]; then
	rm -rf jobs
fi
if [ ! -d jobs ]; then
	mkdir jobs
fi

docker run -p ${jenkins_port}:8080 \
	-v $(pwd)/downloads:/var/jenkins_home/downloads \
	-v $(pwd)/jobs:/var/jenkins_home/jobs/ \
	-v $(pwd)/m2deps:/var/jenkins_home/.m2/repository/ \
	-v $HOME/.ssh:/var/jenkins_home/.ssh/ \
	-d \
	--rm --name ${container_name} \
	${dockerhub_user}/${image_name}:${image_version}